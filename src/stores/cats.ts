import { defineStore } from 'pinia'
import { api } from 'boot/axios'
import { Fact } from 'src/models/Fact'

interface FactRequestPayload {
  fact: {
    value: string
  }
}

const useCatsStore = defineStore('cats', {
  state: () => ({
    facts: [] as Fact[],
    fact: {} as Fact
  }),

  actions: {
    getAllFacts (payload?: object) {
      return new Promise(resolve => {
        if (!payload) {
          payload = {}
        }

        api.get('/v1/cat-facts', {
          params: payload
        })
          .then(resp => {
            this.facts = resp.data.data
            resolve(resp)
          })
      })
    },

    getFact (id: number) {
      return new Promise(resolve => {
        api.get(`/v1/cat-facts/${id}`)
          .then(resp => {
            this.fact = resp.data
            resolve(resp)
          })
      })
    },

    storeFact (payload: FactRequestPayload) {
      return new Promise(resolve => {
        api.post('/v1/cat-facts', { fact: payload.fact.value })
          .then(resp => {
            this.fact = resp.data
            resolve(resp)
          })
      })
    },

    updateFact (id: number, payload: FactRequestPayload) {
      return new Promise(resolve => {
        api.put(`/v1/cat-facts/${id}`, { fact: payload.fact.value })
          .then(resp => {
            this.fact = resp.data
            resolve(resp)
          })
      })
    },

    deleteFact (id: number) {
      return new Promise(resolve => {
        api.delete(`/v1/cat-facts/${id}`)
          .then(resp => {
            resolve(resp)
          })
      })
    }
  }
})

export default useCatsStore
