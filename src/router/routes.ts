import { RouteRecordRaw } from 'vue-router'

// route imports
import GeneralRoutes from './routes/general'
import CatRoutes from './routes/cats'
import DogRoutes from './routes/dogs'

const routes: RouteRecordRaw[] = [
  GeneralRoutes,
  CatRoutes,
  DogRoutes,

  // Always leave this as last one as this is our 404 handler
  {
    path: '/:catchAll(.*)*',
    component: () => import('pages/ErrorNotFound.vue')
  }
]

export default routes
